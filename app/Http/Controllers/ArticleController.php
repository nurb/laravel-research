<?php


namespace App\Http\Controllers;

use App\Article;
use Illuminate\Routing\Controller;


class ArticleController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $article = new Article;
        $article->name = "Harry Potter";
        $article->author = "J Rowling";
        $article->save();

        $articles = ["Hello", "world", "array", "test"];

        return view('article', ['articles' => $articles]);
    }

}